<div class="mg-lg-12">
	<div class="container">

      <div class="wrapper">

        <div class="col col-6">
    
          <div class="panel" style="height: 550px">
                      
            <div class="container-title">
              <h3>Data FPPBJ</h3>
            </div>

            <div class="summary">
              <div class="summary-title">
                Sudah disetujui
                <?php $width = ($fppbj_selesai->num_rows() / $total_fppbj_semua->num_rows()) * 100; ?>
                <span><?php echo $fppbj_selesai->num_rows() ?>/<?php echo $total_fppbj_semua->num_rows() ?></span>
              </div>
              <div class="summary-bars">
                <span class="bar-top is-success" style="width:<?php echo $width ?>%"></span>
                <span class="bar-bottom"></span>
              </div>
            </div>

            <div class="summary">
              <div class="summary-title">
                Belum disetujui 
                <?php $width = ($fppbj_pending->num_rows() / $total_fppbj_semua->num_rows()) * 100;?>
                <span><?php echo $fppbj_pending->num_rows() ?>/<?php echo $total_fppbj_semua->num_rows() ?></span>
              </div>
              <div class="summary-bars">
                <span class="bar-top is-warning" style="width:<?php echo $width ?>%"></span>
                <span class="bar-bottom"></span>
              </div>
            </div>

           <div class="summary">
              <div class="summary-title">
                Tidak Disetujui
                <?php $width = ($fppbj_reject->num_rows() / $total_fppbj_semua->num_rows()) * 100;?>
                <span><?php echo $fppbj_reject->num_rows() ?>/<?php echo $total_fppbj_semua->num_rows() ?></span>
              </div>
              <div class="summary-bars">
                <span class="bar-top is-danger" style="width:<?php echo $width ?>%"></span>
                <span class="bar-bottom"></span>
              </div>
            </div>

            <div class="container-title">
              <h3>Overview FPPBJ</h3>
            </div>

            <div class="is-block">
              <button class="accordion-header">Disetujui <span class="badge is-success"><?php echo $fppbj_selesai->num_rows() ?></span><span class="icon"><i class="fas fa-angle-down"></i></span></button>
              <div class="accordion-panel">
                <?php foreach ($fppbj_selesai->result() as $key) { ?>
                  <p><?= $key->nama_pengadaan ?></p>
                <?php } ?>
              </div>
              <button class="accordion-header">Belum disetujui <span class="badge is-warning"><?php echo $fppbj->num_rows() ?></span><span class="icon"><i class="fas fa-angle-down"></i></span></button>
              <div class="accordion-panel">
                <?php foreach ($fppbj->result() as $key) { ?>
                  <p><?= $key->nama_pengadaan ?></p>
                <?php } ?>
              </div>
              <button class="accordion-header">Tidak disetujui <span class="badge is-danger"><?php echo $fppbj_reject->num_rows() ?></span><span class="icon"><i class="fas fa-angle-down"></i></span></button>
              <div class="accordion-panel">
                <?php foreach ($fppbj_reject->result() as $key) { ?>
                  <p><?= $key->nama_pengadaan ?></p>
                <?php } ?>
              </div>
              
            </div>

          </div>

        </div>

        <div class="col col-6">
          <div class="panel">
            <div class="container-title">
              <h3>Notifikasi</h3>
              <div class="badge is-primary is-noticable">
              {total_notif}
              </div> <!-- SHOW TOTAL NOTIFICATION -->
            </div>
            <div class="scrollbar" id="custom-scroll" style="height: 470px; overflow-x: auto;">
              <!-- LINE NOTIFICATION -->
              <?php foreach ($notification->result() as $key) { ?>
                <div class="notification is-warning"><p><?= $key->value ?></p><a href="<?= site_url('dashboard/delete_notif/'.$key->id) ?>" class="delete delete-notif">X</a></div>
              <?php } ?>
            </div>
          </div>
        </div>
        
        <div class="col col-6">  
          <div class="panel">
            <h4>Data Rekap Perencan aan Tahun <select style="background: none; border: none; border-bottom: 2px #3273dc solid; color: #3273dc; cursor: pointer;" id="yearGraph"><option value="2017">2017</option><option value="2018" selected>2018</option><option value="2019">2019</option></select></h4>
            <div id="graph_" style="height: 500px;">
            </div>
          </div>
        </div>

      </div>

    </div>
</div>

