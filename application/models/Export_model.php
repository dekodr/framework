<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Export_model extends MY_Model{
	public $fppbj   = 'ms_fppbj';
	public $fkpbj   = 'ms_fkpbj';
    public $fp3     = 'ms_fp3';
    public $method  = 'tb_proc_method';
    
	function __construct(){
		parent::__construct();

	}
    
    
    function fppbj($id=''){
        $data = $this->db->where('id', $id)->get($fppbj);

        return $data->row_array();
    }

    function fkpbj($id_fppbj=''){
        $data = $this->db->where('id_fppbj', $id_fppbj)->get($fkpbj);

        return $data->row_array();
    }

    public function get_analisa($id_fppbj){
        $data = $this->db->where('id_fppbj', $id_fppbj)->get('tr_analisa_risiko');
        $data = $data->row_array();
        $data['dpt_list_'] = json_decode($data['dpt_list']);

        // print_r($data);
        if ($data['dpt_list_'] !== null) {
            # code...
            unset($data['dpt_list']);
            foreach ($data['dpt_list_']->dpt as $id_dpt) {
                // echo $id_dpt;
                $data['dpt_list'][] .= $this->get_dpt($id_dpt);
            }
        }
        $data['usulan'] = $data['dpt_list_']->usulan;
        // print_r($data);
        return $data;

    }

    public function get_dpt($id_dpt)
    {
        $data = $this->db->where('id', $id_dpt)->get('ms_vendor')->row_array();
        return $data['name'];
    }

    public function get_sistem_kontrak($id_fppbj,$table)
    {
        // echo $id_fppbj;echo $table;
        if ($table == 'ms_fppbj') {
            $data = $this->db->where('id', $id_fppbj)->get('ms_fppbj');
        } else {
            $data = $this->db->where('id_fppbj', $id_fppbj)->get($table);
        }
        
        $data = $data->row_array();
        // print_r($data);die;
        $data['sistem_kontrak'] = json_decode($data['sistem_kontrak']);

        if ($data['sistem_kontrak'] !== null) {
            foreach ($data['sistem_kontrak'] as $id_fppbj) {
                $data['sistem_kontrak_'][] .= $id_fppbj;
            }
        }
        // print_r($data);
        return $data;
    }

    function rekap_perencanaan($year = null){
        $data = $this->db->select('ms_fppbj.id id_fppbj, year_anggaran, nama_pengadaan, tb_division.name divisi, ms_fppbj.id, idr_anggaran, jenis_pengadaan, tb_proc_method.name metode_pengadaan, desc, jwp, jwpp')
                    ->where('year_anggaran', $year)
                    ->where('is_approved', 3)
                    ->where('is_status', 0)
                    ->where('lampiran_persetujuan IS NOT NULL')
                    ->join('tb_division', 'tb_division.id = ms_fppbj.id_division')
                    ->join('tb_proc_method', 'tb_proc_method.id = ms_fppbj.metode_pengadaan')
                    ->get('ms_fppbj')->result_array();
                    // print_r($data);die;
        return $data;
    }

    function rekap_department($year = null){
        $kadiv = $this->db->select('name kadiv, id')->get('tb_kadiv')->result_array();

        foreach ($kadiv as $key => $value) {
            $data[$key]['id_kadiv'] = $value['id'];
            $data[$key]['kadiv']    = $value['kadiv'];
            # code...
            $division = $this->db
                            ->select('tb_division.name division, id id_division,
                                (SELECT count(id) FROM ms_fppbj where ms_fppbj.id = tb_division.id AND lampiran_persetujuan IS NOT NULL AND is_approved = 3 AND is_status = 0 AND metode_pengadaan = 1) as pelelangan,
                                (SELECT count(id) FROM ms_fppbj where ms_fppbj.id = tb_division.id AND lampiran_persetujuan IS NOT NULL AND is_approved = 3 AND is_status = 0 AND metode_pengadaan = 2) as pemilihan_langsung,
                                (SELECT count(id) FROM ms_fppbj where ms_fppbj.id = tb_division.id AND lampiran_persetujuan IS NOT NULL AND is_approved = 3 AND is_status = 0 AND metode_pengadaan = 3) as swakelola,
                                (SELECT count(id) FROM ms_fppbj where ms_fppbj.id = tb_division.id AND lampiran_persetujuan IS NOT NULL AND is_approved = 3 AND is_status = 0 AND metode_pengadaan = 4) as penunjukan_langsung,
                                (SELECT count(id) FROM ms_fppbj where ms_fppbj.id = tb_division.id AND lampiran_persetujuan IS NOT NULL AND is_approved = 3 AND is_status = 0 AND metode_pengadaan = 5) as pengadaan_langsung')
                            ->where('id_kadiv', $value['id'])
                            ->get('tb_division')
                            ->result_array();

            $data[$key]['detail'] = $division;
        }
        
        return $data;
    }

    function get_exportFP3($id_fppbj=''){
        $data = $this->db->select('ms_fppbj.nama_pengadaan nama_pengadaan_fppbj, ms_fp3.nama_pengadaan, tb_proc_method.name metode_pengadaan, ms_fp3.jadwal_pengadaan, ms_fp3.desc, ms_fp3.status, tb_division.name nama_divisi')
                        ->where('ms_fp3.id_fppbj', $id_fppbj)
                        ->join('ms_fppbj', 'ms_fppbj.id = ms_fp3.id_fppbj', 'LEFT')
                        ->join('tb_proc_method', 'tb_proc_method.id = ms_fp3.metode_pengadaan', 'LEFT')
                        ->join('tb_division', 'tb_division.id = ms_fppbj.id_division', 'LEFT')
                        ->group_by('ms_fp3.id')
                        ->get('ms_fp3');
                        
                        
        // print_r($data->result_array());die;
        return $data->result_array();
    }

    function get_exportUser(){
        $data = $this->db->select('ms_user.*, tb_division.name division')
                    ->where('ms_user.del', 0)
                    ->join('tb_division', 'tb_division.id = ms_user.id_division')
                    ->get('ms_user')->result_array();

        return $data;
    }

    
}