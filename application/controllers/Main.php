<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('pdf');
		include_once APPPATH.'third_party/dompdf2/dompdf_config.inc.php';

		$this->load->model('Main_model', 'mm');

	}
	public function index(){

		/*
		| -------------------------------------------------------------------
		|  Authentication users login
		| -------------------------------------------------------------------
		| These function prevented visitor to open restricted page.
		|
		| Prototype:
		|
		|	if($this->session->userdata('user')){
		|		redirect(site_url('dashboard'));
		|	}else{
		|		$this->login();
		|	}
		|
		*/
		// print_r($this->session->userdata());
		if($this->session->userdata('admin')){
			redirect(site_url('dashboard'));
		}else{
			$this->login();
		}
	}

	public function login(){
		if($this->session->userdata('admin')){
			// print_r($this->session->userdata());die;
			redirect(site_url('dashboard'));
		}else{
			
			$this->load->view('template/layout-login-nr');
		}
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect(site_url());
	}

	public function check(){
		$return['status'] = 'error';
		$return['message']= 'Terjadi Kesalahan';
		$result = $this->mm->check($this->input->post());
		
		if($result){
			// $return['status'] = 'success';
			$message1 = "Login Berhasil";
			echo "<script type='text/javascript'>alert('$message1');</script>";
			// $return['url'] = site_url('dashboard');
			redirect('dashboard');

		}else{
			// $return['status'] = 'error';
			 $message = "Username atau Password salah";
			 echo "<script type='text/javascript'>alert('$message');</script>";
			// echo "<div class='login-notif' id='gagal'>Data tidak dikenal. Silahkan login kembali!</div>";
			 $this->load->view('template/layout-login-nr');
		}


		// echo json_encode($return);
	}	
	public function custom_query(){
		$this->mm->custom_query();
	}

	public function update_status(){
		$id_fppbj 	= $_GET['id_fppbj'];
		$param_ 	= $_GET['param_'];
		print_r($id_fppbj);
		$this->mm->update_status('ms_fppbj', $id_fppbj, $param_);
	}

	public function search(){
		$q = $_GET['q'];
		$data = $this->mm->search($q);
		
	}
	
	function get_dpt_csms($csms){
		$data = $this->db->select('ms_vendor.name vendor, ms_vendor.id id_vendor, ms_score_k3.score, value csms')
						->where('id_csms_limit', $csms)
						->where('ms_vendor.vendor_status', 2)
						->join('ms_vendor', 'ms_vendor.id = ms_score_k3.id_vendor')
						->join('tb_csms_limit', 'tb_csms_limit.id = ms_score_k3.id_csms_limit')
						->get('ms_score_k3');
		
		echo json_encode($data->result_array());
		return json_encode($data->result_array());
	}

	function get_dpt_fkpbj(){
		$data = "SELECT 
						name ,
						id
				 FROM 
				 		ms_vendor 
				 WHERE 
				 		del =0 AND name
				 LIKE ? ";
		$query = $this->db->query($data,array('%'.$_POST['search'].'%',))->result_array();
		foreach($query as $key => $value){
	        $data[$value['id']] = $value['name'];
	    }
	    return $data;
		
	}

	function rekapPerencanaanGraph($year){
		// $data['year']	= $year;
		$data	= $this->mm->rekapPerencanaanGraph($year);
		print_r($data);
		return $data;
	}
}